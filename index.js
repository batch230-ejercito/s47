const txtFirstName = document.querySelector('#txt-first-name');
const spanFullname = document.querySelector('#span-full-name');
const spanGreetings= document.querySelector('.span-greetings');

// Alternatives for document.querySelector()

/*
	document.getElementById('txt-firstName');
	document.getElementByClassName('txt-inputs')
	document.getElementByTagName('input');

*/

txtFirstName.addEventListener('keyup', (event)=>{ 
	spanFullname.innerHTML = txtFirstName.value;


});


txtFirstName.addEventListener('keyup', (event)=>{ 
	console.log(event.target);
	console.log(event.target.value);  // when trying to add value it will console 
});


/*
		event listener


*/
// additional examples 
const greetings = () =>{
		let firstName = txtFirstName.value;
		let greeting = 'Hi, welcome!' ;
		spanGreetings.innerHTML = 'Hi, welcome!' + firstName;


};

txtFirstName.addEventListener('keyup', greetings);

// additional example & Mini Activity

